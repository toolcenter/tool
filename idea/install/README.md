### IDEA 安装工具

1_idea所有产品激活.rar：全版本自动激活脚本，支持 2017 2018 2019 2020 2021 2022，激活至 2099 年。

jetbrains-agent.jar，激活至 2089 年，最高支持版本到 2020.1（包含）。

ide-eval-resetter-2.3.5.jar，无限重置 30 天试用插件。`2021.2.2`及以下版本很好用；`2021.3` 以下（不含）堪堪能用，需要配合一些手法；`2021.3` 版本开始正式失效。

ide-eval-resetter 下载原始地址：https://plugins.zhile.io/files/ide-eval-resetter-2.3.5-c80a1d.zip

reset_jetbrains_eval_mac_linux.sh，Linux 和 macOS 系统的重置 30 天试用天数脚本。

reset_jetbrains_eval_windows.vbs，Windows 系统的重置 30 天试用天数脚本。