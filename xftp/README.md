# Xftp

最新版下载地址（非商业使用免费）：[https://www.netsarang.com/en/free-for-home-school/](https://www.netsarang.com/en/free-for-home-school/)

填写 name（随便填） 和 email（需要填真实的），随后会把下载最新版的下载链接发到你填写的邮箱里。

选择 Both（表示获取最新版的 Xshell 和 Xftp 的下载链接）。

不定时对获取到的版本放到仓库中进行镜像，方便下载：

2022 年 5 月 25 日：Xshell-7.0.0109p.exe、Xftp-7.0.0107p.exe

---

**Update: As of 02/16/2022 the tab limits for our Free Licenses have been removed. All free users can now access unlimited tabs by downloading the latest build below or updating your existing clients.**

自从 2022 年 2 月 16 日起，免费版本的标签栏限制已经被移除了。所有免费使用的用户现在可以通过下载最新版本或者更新已有版本的客户端来解除标签栏的限制。

