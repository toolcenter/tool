# dex2jar

安卓 .dex 文件反编译工具

GitHub 源码地址：[https://github.com/pxb1988/dex2jar](https://github.com/pxb1988/dex2jar)

GitHub 下载地址：[https://github.com/pxb1988/dex2jar/releases](https://github.com/pxb1988/dex2jar/releases)