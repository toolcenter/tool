# FastCopy 工具介绍

FastCopy 是 Windows 平台用于快速复制或移动文件的工具。

按博主的使用来看，FastCopy很适合用于硬盘对拷和大文件夹的移动，速度比 Windows 的复制移动要快的多，也比 DiskGenius 的分区克隆要快。



一般使用只要明白以下几个参数即可：

- Source：源文件（夹），就是你想复制的东西。
- DestDir：目标文件（夹），就是你想复制到哪。
- Diff、Copy、Move、Sync、Delete All
  - diff：复制，有不同策略，一般默认 (Size/Date)
  - copy：复制，并且覆盖同名文件
  - move：剪切
- Buffer (MB)：缓存大小，内存充足的情况 512 MB 能比较快速
- Full Speed：速度，默认满速
- Nonstop：复制时不会息屏
- Verify：数据检验，对于重要文件建议开启，也影响不了多少速度
- Estimate：时间估计
- Execute：执行复制或移动





> 参考资料：
>
> [【工具介绍】fastcopy的下载与使用方法，可用于硬盘对拷](https://blog.csdn.net/j84491135/article/details/120235831)



