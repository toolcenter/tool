# 格式工厂

## 版本说明

- 5.11.0.0：最后一个没有引入【BrightData】的版本，后面的开始引入所谓的白金功能了。下载源：[http://down.pcgeshi.com/FormatFactory_5.11.0.0_setup.exe](http://down.pcgeshi.com/FormatFactory_5.11.0.0_setup.exe)
- 4.9.5.0：支持 32 位操作系统。下载源：[http://down.pcgeshi.com/FormatFactory_4.9.5.exe](http://down.pcgeshi.com/FormatFactory_4.9.5.exe)
- 3.8.0.0：支持 RMVB 格式。下载源：[http://down.pcgeshi.com/FormatFactory_3.8.0.0_setup.exe](http://down.pcgeshi.com/FormatFactory_3.8.0.0_setup.exe)

官网下载地址：[http://www.pcgeshi.com/download.html](http://www.pcgeshi.com/download.html)



由于格式工厂从【5.12.0.0】版本开始引入了【BrightData】如果你想使用高级功能，需要开启这个，开启的时候会出现以下提示。【5.12.2.0】没有任何提示，安装完后直接就出来了，而且直接开机自启了，而且无法在设置中关掉！而且这个东西会开机自启，这东西是用来收集用户的信息的，而且会把你电脑上作为代理 IP 卖给其他公司，把你电脑作为一台肉鸡，去攻击别人的服务器，在你不知情的情况下变成 ddos 攻击的帮凶！

![image-20220910143256264](./pictures/README/image-20220910143256264.png)



> 参考文献：
>
> [格式工厂竟隐藏Birght data，用心何在？](https://www.bilibili.com/video/BV19B4y1G7Fs)
>
> [以色列公司“BRIGHT DATA”（LUMINATI NETWORKS）支持对卡拉帕坦的攻击](https://www.wangan.com/p/7fygf763cd3aacaf)