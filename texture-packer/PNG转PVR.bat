rem 请核对你的texturepacker安装路径
@echo off
 
path %path%;"C:\Program Files\CodeAndWeb\TexturePacker\bin"
 
for /f "usebackq tokens=*" %%d in (`dir /s /b *.png`) do (
TexturePacker.exe "%%d" --sheet "%%~dpnd.pvr.ccz" --opt RGB888 --allow-free-size --algorithm Basic --no-trim --dither-fs
TexturePacker.exe "%%d" --sheet "%%~dpnd_alpha.pvr.ccz" --opt ALPHA --allow-free-size --algorithm Basic --no-trim --dither-fs
)

del out.plist

pause
