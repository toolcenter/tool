# TexturePacker-4.8.1

## 破解步骤

解压文件，默认安装到 `C:\Program Files\CodeAndWeb\TexturePacker\bin`，然后替换掉原来的 `TexturePacker.exe` 和 `TexturePackerGUI.exe`。



## 使用步骤

在 `C:\Program Files\CodeAndWeb\TexturePacker\bin` 下新建一个文件夹 `png2pvr`，然后把要转换的 png 图片还有 `PNG转PVR.bat` 脚本放到一起，执行脚本。

> 由于默认情况下安装在 C 盘，在这个目录下执行脚本有可能因为权限问题导致无法执行。这个时候可以在其他目录下去执行脚本，安装的软件不用动（脚本中指定了软件的位置）。