# dev-sidecar

开发者边车，命名取自service-mesh的service-sidecar，意为为开发者打辅助的边车工具（以下简称ds）
通过本地代理的方式将https请求代理到一些国内的加速通道上。

下载地址：[https://github.com/docmirror/dev-sidecar/releases](https://github.com/docmirror/dev-sidecar/releases)



1.7.3

这大概是最后一个版本了，大家且下且珍惜。https://www.zhihu.com/question/498939985

更新内容：
1、windows下用一种巧妙的方法正确设置https代理，解决pip install报错的问题。
2、增加服务端监听ip设置
3、升级Electron版本为17.x

安装前请仔细阅读安装说明

| 平台          | 安装说明                                                     |
| ------------- | ------------------------------------------------------------ |
| 【Windows】   | 下载后提示无法验证发行者时，选择保留即可 注意：开着ds重启电脑会导致无法上网，你可以再次打开ds，然后右键小图标退出ds即可。[更多说明](https://github.com/docmirror/dev-sidecar/issues/109) |
| 【Mac】       | 安装时提示无法验证开发者时，请先取消 然后去系统偏好设置->安全与隐私->下方已阻止使用DevSidecar 选择仍要打开 |
| 【Ubuntu】    | [安装说明](https://gitee.com/docmirror/dev-sidecar/blob/master/doc/linux.md) |
| 【其他Linux】 |                                                              |

