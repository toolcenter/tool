# Navicat

## Navicat 破解/重置说明

Navicat 有两种方式无限使用：

1、使用破解工具破解，破解后不再弹出注册弹窗，带永久许可证那种。（但是很难保证会不会破解软件有没有后门，而且软件升级了破解工具可能就失效了，需要破解工具也进行升级。）

2、无限重置试用，配合重置工具 + 设置定时任务每天自动执行，可以实现无限试用的效果。但是每次打开的时候都会出现使用提醒，评估版本。（安全，而且只是重置了使用并没有真正破解，避免法律纠纷，理论上支持所有 Navicat 的版本。）



选择其中一种重置方式即可，推荐用重置脚本的方式。

重置的方式理论上支持所有的 Navicat 版本。

重置脚本：[https://github.com/samho2008/reset-navicat-premium](https://github.com/samho2008/reset-navicat-premium)

重置软件：[https://github.com/samho2008/navicat-premium-crack](https://github.com/samho2008/navicat-premium-crack)



> 参考文献：
>
> [navicat16破解_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1j5411o7Nz)



## Navicat 官网

中文官网：[https://navicat.com.cn/products](https://navicat.com.cn/products)

英文官网：[https://www.navicat.com/en/products](https://www.navicat.com/en/products)



## Navicat 下载地址

中文官网：[https://navicat.com.cn/download/navicat-premium](https://navicat.com.cn/download/navicat-premium)

英文官网：[https://www.navicat.com/en/download/navicat-premium](https://www.navicat.com/en/download/navicat-premium)



## Navicat 版本更新日志

更新日志：[https://www.navicat.com.cn/products/navicat-premium-release-note](https://www.navicat.com.cn/products/navicat-premium-release-note)



## 下载 Navicat 历史安装包

官网只能下载最新版本，并且没有直接提供历史版本下载的选项。

那么，怎么获取以前版本的官方安装包呢？

在官方下载最新版本的安装包，目前最新的版本是 16，观察下载路径：[https://download.navicat.com.cn/download/navicat160_premium_cs_x64.exe](https://download.navicat.com.cn/download/navicat160_premium_cs_x64.exe)

观察到，里面的变量是 160，对应 16 的版本。

其中，cs 对应 Chinese，中文版。en 对应 English，英文版。

x64，表示 64 位系统。

所以，如果想下载 15 的版本，那就把 160 改成 150 就好了。

但是，这个渠道有可能官方以后会封掉，所以需要赶紧把以前的安装包备份一份。

Navicat 16：

中文：[https://download.navicat.com.cn/download/navicat160_premium_cs_x64.exe](https://download.navicat.com.cn/download/navicat160_premium_cs_x64.exe)

英文：[https://download3.navicat.com/download/navicat160_premium_en_x64.exe](https://download3.navicat.com/download/navicat160_premium_en_x64.exe)



Navicat 15：

中文：[https://download.navicat.com.cn/download/navicat150_premium_cs_x64.exe](https://download.navicat.com.cn/download/navicat150_premium_cs_x64.exe)

英文：[https://download3.navicat.com/download/navicat150_premium_en_x64.exe](https://download3.navicat.com/download/navicat150_premium_en_x64.exe)



Navicat 12：

中文：[https://download.navicat.com.cn/download/navicat120_premium_cs_x64.exe](https://download.navicat.com.cn/download/navicat120_premium_cs_x64.exe)

英文：[https://download3.navicat.com/download/navicat120_premium_en_x64.exe](https://download3.navicat.com/download/navicat120_premium_en_x64.exe)

